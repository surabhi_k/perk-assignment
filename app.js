var app = angular.module("perk_app", []);

app.controller('perk_controller', function($scope, $window) {

    // variables
    $scope.users = [];
    $scope.edit_user = [];
    $scope.new_user = [];
    $scope.check = true;

    $scope.users = usersResponses.data.users;

    //methods
    $scope.uuid = function() {
        return $scope.s4() + $scope.s4() + '-' + $scope.s4() + '-' + $scope.s4() + '-' + $scope.s4() + '-' + $scope.s4() + $scope.s4() + $scope.s4();
    };

    $scope.s4 = function() {
        return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
    };

    $scope.clear_form = function() {
        $scope.form.$setPristine();
        $scope.id = null,
            $scope.email = null,
            $scope.first_name = null,
            $scope.last_name = null,
            $scope.passwd = null,
            $scope.confrm_passwd = null,
            $scope.is_suspended = null
    };

    $scope.addUser = function() {
        if ($scope.passwd == $scope.confrm_passwd) {
            $scope.check = true;
            $scope.id = $scope.uuid();
            $scope.users.push({
                id: $scope.id,
                email: $scope.email,
                first_name: $scope.first_name,
                last_name: $scope.last_name,
                passwd: $scope.passwd,
                confrm_passwd: $scope.confrm_passwd,
                is_suspended: false
            });
            $scope.new_user = $scope.user;
            $('#myModal').modal('hide');
            $window.localStorage.setItem('users', JSON.stringify($scope.users));
            $scope.clear_form();
        } else {
            $scope.check = false;
        }
    };

    $scope.editUser = function(user) {
        for (var i in $scope.users) {
            if ($scope.users[i].id == user.id) {
                $scope.users[i] = user;
                $scope.edit_user = user;
            }
        }
        $window.localStorage.setItem('users', JSON.stringify($scope.users));
    };

});